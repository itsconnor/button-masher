﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio; // this is needed for sound effects
using Microsoft.Xna.Framework.Media; //this is needed for song
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace QuickDraw
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Loaded in the game assests 
        Texture2D buttonTexture;
        SpriteFont mainSpriteFont;
        SoundEffect clickSFX;
        SoundEffect gameEndSFX;
        SoundEffect FireSFX;
        Song gameMusic;

        //Game State / inputs
        MouseState previousState;
        int score = 0;
        bool playing = false;
        float Reflextime = 0f;
        float timeLimit = 5f;
        Random rnd = new Random();
        float secretTimer = 0f;
        string message = "";
        bool click = false;
        bool draw = false;




        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //this is where we will load out button texture
            buttonTexture = Content.Load<Texture2D>("button");

            //load game font
            mainSpriteFont = Content.Load<SpriteFont>("mainSpriteFont");

            //
            FireSFX = Content.Load<SoundEffect>("Fire");
            //load SFFX
            clickSFX = Content.Load<SoundEffect>("buttonClick");

            //load SFFX
            gameEndSFX = Content.Load<SoundEffect>("gameover");

            //this is the song that will play in the backround
            gameMusic = Content.Load<Song>("music");

            //starting the backround music
            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;

            //make the mouse visable
            IsMouseVisible = true;


        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            MouseState currentState = Mouse.GetState();
            //this is to find the center of the screen
            Vector2 screenCenter = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);
            //determine button rectangle
            Rectangle buttonRect = new Rectangle((int)screenCenter.X - buttonTexture.Width / 2,
                                                (int)screenCenter.Y - buttonTexture.Height / 2, buttonTexture.Width, buttonTexture.Height);
            //check if the user has clicked the mouse

            if (currentState.LeftButton == ButtonState.Pressed && previousState.LeftButton != ButtonState.Pressed && buttonRect.Contains(currentState.X, currentState.Y))
            {
                //iff the mouse is pressed 
                clickSFX.Play();
                
                // this adds to our score
                if (playing == true && secretTimer > 0)
                { 
                    playing = false;
                    message = ("you clicked to early");
                }
                if(!playing && secretTimer == 0)
                {
                    secretTimer = rnd.Next(3, 11);
                    playing = true;

                }
                if (draw == true) 
                {
                    click = true;
                    message = "your time is " + Reflextime;
                }
            }


            if (playing == true)
            {
                
                secretTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;


                if (secretTimer <= 0)
                {
                   

                    secretTimer = 0;

                   
                    FireSFX.Play();
                    draw = true;

                }
                if (draw && click == false)
                {
                    Reflextime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
             
                if (playing == false)
                {
                    gameEndSFX.Play();
                }
                    previousState = currentState;


                base.Update(gameTime);
            }
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightGoldenrodYellow);

            // TODO: Add your drawing code here

            //start drawing
            spriteBatch.Begin();
            MouseState currentState = Mouse.GetState();
            //this is to find the center of the screen
            Vector2 screenCenter = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);

            //this is where we will draw the sprites
            spriteBatch.Draw(buttonTexture, new Rectangle((int)screenCenter.X - buttonTexture.Width / 2, (int)screenCenter.Y - buttonTexture.Height / 2
                                                            , buttonTexture.Width, buttonTexture.Height), Color.Green);

            string promptString = "click the button to start!";
            if (playing == true)
                promptString = "Click the Button when the alert goes";
            Vector2 promptSize = mainSpriteFont.MeasureString(promptString);

            Vector2 gameoversize = mainSpriteFont.MeasureString("Well done you got   ");
            if (playing == false)
                spriteBatch.DrawString(mainSpriteFont, message, screenCenter - new Vector2(0, 200) - gameoversize / 2, Color.Black);
            
            //draw text

            Vector2 Namesize = mainSpriteFont.MeasureString("By Connor Turner");

            spriteBatch.DrawString(mainSpriteFont, "welcome to Quick Draw " , new Vector2(500, 60), Color.Black);

            spriteBatch.DrawString(mainSpriteFont, message, new Vector2(500, 80), Color.Black);

            //spriteBatch.DrawString(mainSpriteFont, "Time Left: ", new Vector2(10, 60), Color.Black);
            //spriteBatch.DrawString(mainSpriteFont, secretTimer.ToString(), new Vector2(100, 60) , Color.Black);

            //spriteBatch.DrawString(mainSpriteFont, "Time Left: ", new Vector2(10, 100), Color.Black);
            //spriteBatch.DrawString(mainSpriteFont, Reflextime.ToString(), new Vector2(100, 100), Color.Black);

            spriteBatch.DrawString(mainSpriteFont, "By Connor Turner", screenCenter - new Vector2(0, 80) - Namesize / 2, Color.Black);
            spriteBatch.DrawString(mainSpriteFont, promptString, screenCenter - new Vector2(0, 60) - promptSize / 2, Color.Black);

            //stop drawing with
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}